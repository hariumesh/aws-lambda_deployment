 provider "aws" {
  }


 resource "aws_lambda_function" "example" {
   function_name = "ServerlessGitlabTerraform"

   # The bucket name as created earlier with "aws s3api create-bucket"
   s3_bucket = "umesh-idp-terraform-serverless"
   s3_key    = "hello.zip"

   # "main" is the filename within the zip file (main.js) and "handler"
   # is the name of the property under which the handler function was
   # exported in that file.
   handler = "hello.handler"
   runtime = "python3.7"
   role    = "arn:aws:iam::776454489480:role/idp_test_lambda"

   #role = aws_iam_role.lambda_exec.arn
 }

